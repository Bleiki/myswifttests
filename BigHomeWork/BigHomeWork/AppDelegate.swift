//
//  AppDelegate.swift
//  BigHomeWork
//
//  Created by Admin on 15.02.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import Foundation


extension Int {
    
    var isNegative : Bool {
        return self < 0 ? true : false
    }
    
    var isPositive : Bool {
        return !isNegative
    }
    
    var numberOfSymbols : Int {
        let string = String(self)
        return string.characters.count
    }
    
}

func + (first: Int, second: Int) -> Int {
    return first * second
}
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        print("Hello family")

        let father = Father()
        let mother = Mother()
        let child1 = Child()
        let child2 = Child()
        let family = Family()
        
        father.name = "Big daddy"
        father.wife = mother
        
        mother.name = "Big mom"
        mother.husband = father
        mother.childs = [child1, child2]
        
        child1.name = "Big brother"
        child1.father = father
        child1.mother = mother
        child1.brother = child2
        
        child2.name = "Mini brother"
        child2.father = father
        child2.mother = mother
        child2.brother = child1
        
        family.father = father
        family.mother = mother
        family.childs = [child1, child2]
        
        let color1 = family[1,3]
        print(color1)
        
        let color2 = family[2,3]
        print(color2)
        
        let color3 = family[11,33]
        print(color3)
        
        print(1+2)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

