//
//  Family.swift
//  BigHomeWork
//
//  Created by Admin on 16.02.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation
import UIKit

class Family {
    
    var father : Father!
    var mother : Mother!
    var childs : [Child]!
    
    deinit {
        print("Family  die :(")
    }
    
    subscript(row: Int, column: Int) -> String {
        
        if row > 0 && row < 9 && column > 0 && column < 9 {
            return row % 2 == column % 2 ? "Black" : "White"
        } else {
            return "Ne popal"
        }
        
    }
}

class Human {
    
    var name = ""
    
}

class Father : Human {
    
    var wife : Mother?
    
    deinit {
        print("\(self.name) die :(")
    }
}

class Mother : Human {
    
    weak var husband : Father!
    var childs  : [Child]!
    
    deinit {
        print("\(self.name) die :(")
    }
}

class Child : Human {
    
    weak var mother  : Mother!
    weak var father  : Father!
    weak var brother : Child!
    
    deinit {
        print("\(self.name) die :(")
    }
}

