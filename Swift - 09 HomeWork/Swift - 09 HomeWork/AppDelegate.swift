//
//  AppDelegate.swift
//  Swift - 09 HomeWork
//
//  Created by Admin on 20.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
 
        var vowels     = 0
        var symbols    = 0
        var consonants = 0
        let myString   = "Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state. Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game."
        
        for char in myString.characters {
            switch char {
                case "e", "y", "u", "i", "o", "a":
                vowels += 1
                
                case "q","w","r","t","p","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m":
                consonants += 1
                
                default:
                symbols += 1
            }
        }
        
        print("vowels: \(vowels)\nsymbols: \(symbols)\nconsonants: \(consonants)\ncount: \(myString.characters.count)")
        
        
        
        let student    = (firstName: "Вячеслав", patronymic: "Игоревич", lastName: "Герасименок")
        
        switch student {
            case _ where student.firstName.hasPrefix("А") || student.firstName.hasPrefix("О"):
            print(student.firstName)
            
            case _ where student.patronymic.hasPrefix("В") || student.patronymic.hasPrefix("Д"):
            print("\(student.firstName) \(student.patronymic)")
            
            case _ where student.lastName.hasPrefix("Е") || student.lastName.hasPrefix("З"):
            print(student.lastName)
            
            default:
            print("\(student.firstName) \(student.patronymic) \(student.lastName)")
        }
        
        
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

