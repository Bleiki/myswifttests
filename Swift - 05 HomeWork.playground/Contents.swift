

let numberOfSec = 30 * 24 * 60 * 60
print(numberOfSec)

let firstQuarter = 90 * 24 * 60 * 60
let secondQuarter = firstQuarter + 91 * 24 * 60 * 60
let thirdQuarter = secondQuarter + 92 * 24 * 60 * 60
let fourthQuarter = thirdQuarter + 92 * 24 * 60 * 60

if numberOfSec <= firstQuarter {
    print("I born in first quarter")
} else if numberOfSec > firstQuarter && numberOfSec <= secondQuarter {
    print("I born in second quarter")
} else if numberOfSec > secondQuarter && numberOfSec <= thirdQuarter {
    print("I born in third quarter")
} else {
    print("I born in fourth quarter")
}

let a = 3
let b = 1

if a > 0 && a < 9 && b > 0 && b < 9 {
    
    if ((a + b) % 2 == 0) {
        print("Cell \(a):\(b) is black!!!")
    } else {
        print("Cell \(a):\(b) is white!!!")
    }
    
} else {
    print("Cell \(a):\(b) does not lie on the chessboard!!!")
}