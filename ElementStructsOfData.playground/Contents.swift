//: Playground - noun: a place where people can play

import UIKit

//Stack : LILO

struct Stack<T> {
    
    var container = [T]()
    
    var top: T? {
        return container.last
    }
    
    var isEmpty: Bool {
        return container.isEmpty
    }
    
    var size: Int {
        return container.count
    }
    
    mutating func push(elem: T) {
        container.append(elem)
    }
    
    mutating func pop() -> T? {
        if !isEmpty {
            return container.removeLast()
        }
        
        return nil
    }
}


//Queue : FIFO

struct Queue<T> {
    
    var container = [T]()
    
    var isEmpty: Bool {
        return container.isEmpty
    }
    
    var size: Int {
        return container.count
    }
    
    var head: T? {
        return container.first
    }
    
    var tail: T? {
        return container.last
    }
    
    mutating func enqueue(elem: T) {
        container.append(elem)
    }
    
    mutating func dequeue() -> T? {
        if !isEmpty {
            return container.remove(at: 0)
        }
        
        return nil
    }
}


//Deque : Двусторонняя очередь

struct Deque<T> {
    
    var container = [T]()
    
    var isEmpty: Bool {
        return container.isEmpty
    }
    
    var size: Int {
        return container.count
    }
    
    var head: T? {
        return container.first
    }
    
    var tail: T? {
        return container.last
    }
    
    mutating func addFirst(elem: T) {
        container.insert(elem, at: 0)
    }
    
    mutating func addLast(elem: T) {
        container.append(elem)
    }
    
    mutating func removeFirst() -> T? {
        if !isEmpty {
            return container.remove(at: 0)
        }
        
        return nil
    }
    
    mutating func removeLast() -> T? {
        if !isEmpty {
            return container.remove(at: size - 1)
        }
        
        return nil
    }
}


//LinkList : Связанный список. Односвязный: каждый элемент указывает на последующий, а если за  ним нет элемента, то он указывает на nil. Двухсвязный: каждый элемент указывает на последующий и предыдущий элементы, а если за или перед ним нет элемента, то он указывает на nil.

class Node<T: Equatable> {
    
    var item: T
    var next: Node<T>?
    
    init(elem: T) {
        self.item = elem
    }
    
    func add(newItem: T) {
        if let nextNode = next {
            nextNode.add(newItem: newItem)
        } else {
            next = Node<T>(elem: newItem)
        }
    }
}

class LinkedList<T: Equatable> {
    
    var head: Node<T>?
    
    var size: Int {
        var current = head
        var count   = 0
        
        while current != nil {
            current = current?.next
            count += 1
        }
        
        return count
    }
    
    func add(item: T) {
        
        if head != nil {
           head!.add(newItem: item)
        } else {
            head = Node<T>(elem: item)
        }
    }
    
    func remove(item: T) {
        
        if head != nil {
            
            if head!.item == item {
                
                head = head!.next
                
            } else {
                
                var current = head
                var prev    = head
                
                while current!.next != nil && current!.item != item {
                    prev = current!
                    current = current!.next
                }
                
                if let deleted = current {
                    if deleted.item == item {
                        if let nexter = deleted.next {
                            prev?.next = nexter
                            deleted.next = nil
                        } else {
                            prev?.next = nil
                        }
                    }
                }
            }
        }
    }
}

extension LinkedList {
    
    func print() -> String {
        
        var str = ""
        var current = head
        
        while current != nil {
            str += "\(current!.item) => "
            current = current!.next
        }
        
        return str
    }
    
}
