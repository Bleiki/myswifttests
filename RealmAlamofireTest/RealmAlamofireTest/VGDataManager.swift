//
//  VGDataManager.swift
//  RealmAlamofireTest
//
//  Created by Admin on 04.03.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift


let realm = try! Realm()

class VGDataManager {
    
    static let sharedManager = VGDataManager()
    var currentAccessToken   = VGAccessToken()
    
    //MARK: - GetMethods
    func getFriendsFromServer(ID: Int?, count: Int, offset: Int, success: @escaping (_ friends: [VGFriend]) -> (), failure : @escaping (_ error: Error?) -> ()) {
        
        if let userID = ID {
            
            let url = "https://api.vk.com/method/friends.get"
            let params : [String : Any] = ["user_id" : userID,
                                           "count"   : count,
                                           "offset"  : offset,
                                           "order"   : "name",
                                           "fields"  : "photo_50"]
            
            Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
                if response.result.isSuccess {
                    if let arrayOfDict = ((response.result.value as? [String : Any])?["response"] as? [[String : Any]]), !arrayOfDict.isEmpty {

                        var friends = [VGFriend]()
                        
                        for i in 0..<arrayOfDict.count {
                            
                            let friend = VGFriend(response: arrayOfDict[i])
                            
                            friends.append(friend)
                        }
                        
                        self.saveFriends(friends: friends)
                        
                        success(friends)
                        
                    } else {
                        failure(nil)
                    }
                    
                } else {
                    failure(response.error)
                }
            }
        }
    }
    
    func getImageOfFriend(imageURL: String, success: @escaping (_ image: UIImage) -> (), failure: @escaping (_ error: Error?) -> ()) {
        
        Alamofire.request(imageURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).response { (response) in
                
            if let data = response.data {
                
                let image = UIImage(data: data) ?? UIImage()
                success(image)
                
            } else {
                failure(response.error)
            }
        }
    }
    
    func getUser(userID: Int, success: @escaping (_ user: VGFriend) -> (), failure: @escaping (_ error: Error?) -> ()) {
        
        let url = "https://api.vk.com/method/users.get"
        let params : [String : Any] = ["user_ids" : userID,
                                       "fields"   : "photo_50"]
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            if response.result.isSuccess {
                print(response)
                if let arrayOfParams = (response.result.value as? [String : Any])?["response"] as? [[String : Any]], !arrayOfParams.isEmpty {
                    
                    let newFriend = VGFriend(response: arrayOfParams.first!)
                    
                    success(newFriend)
                    
                } else {
                    failure(nil)
                }
                
            } else {
                failure(response.error)
            }
        }
    }
    
    
    //MARK: - OtherMethods
    func autorizeUser(success: @escaping (_ result: Bool) -> ()) {
        
        let vc = AutorizeViewController()
        vc.clouser = {(token) in
            self.currentAccessToken = token
            success(true)
        }
        
        let nav    = UINavigationController(rootViewController: vc)
        let mainVC = UIApplication.shared.windows.first?.rootViewController
        mainVC?.present(nav, animated: true, completion: nil)
    }
    
    func deleteFriend(userID: Int, success: @escaping (_ result: Bool) -> ()) {
        
        if let token = currentAccessToken.token {
            
            let url = "https://api.vk.com/method/friends.delete"
            let params : [String : Any] = ["user_id"      : userID,
                                           "access_token" : token]
            
            Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                success(response.result.isSuccess)
            }
        }
    }
    
    func addFriend(userID: Int?, success: @escaping (_ result: String) -> (), failure: @escaping (_ error: Error?) -> ()) {
        
        if let ID = userID, let token = currentAccessToken.token {
            
            let submittedValue = 1
            let approvedValue  = 2
            let resendingValue = 4
            let url = "https://api.vk.com/method/friends.add"
            let params : [String : Any] = ["user_id"      : ID,
                                           "access_token" : token]
            
            Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                
                if response.result.isSuccess {
                    print(response)
                    if let value = (response.result.value as? [String : Any])?["response"] as? Int {
                        
                        switch value {
                        case submittedValue:
                            success("Application for adding this user as a friend has been sent.")
                        case approvedValue:
                            success("Friend request from this user is approved.")
                        case resendingValue:
                            success("Resubmit an application.")
                        default:
                            failure(nil)
                        }
                        
                    } else {
                        failure(nil)
                    }
                    
                } else {
                    failure(response.error)
                }
            }
        } else {
            failure(nil)
        }
    }
    
    
    //MARK: - RealmMethods
    func saveFriends(friends: [VGFriend]) {
        
        do {
            try realm.write {
                realm.add(friends, update: true)
            }
        } catch {
            print("Can't save friends!!!")
        }
    }
}

