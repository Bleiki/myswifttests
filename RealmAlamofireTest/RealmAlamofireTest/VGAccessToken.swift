//
//  VGAccessToken.swift
//  RealmAlamofireTest
//
//  Created by Admin on 11.03.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation

class VGAccessToken {
    
    var token          : String?
    var userID         : Int?
    var expirationDate : NSDate?
}
