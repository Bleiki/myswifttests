//
//  VGFriend.swift
//  RealmAlamofireTest
//
//  Created by Admin on 04.03.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class VGFriend : Object {
    
    dynamic var firstName = ""
    dynamic var lastName  = ""
    dynamic var imageURL : String? = nil
    dynamic var id : Int = 0
    
    init(response: [String : Any]){
        super.init()
        
        firstName = response["first_name"] as? String ?? "No firstName"
        lastName  = response["last_name"] as? String ?? "No lastName"
        imageURL  = response["photo_50"] as? String
        id        = response["user_id"] as? Int ?? 0
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init() {
        super.init()
    }

    override static func primaryKey() -> String? {
        return "id"
    }
}
