//
//  FriendsTableViewController.swift
//  RealmAlamofireTest
//
//  Created by Admin on 05.03.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import Foundation

class FriendsTableViewController: UITableViewController {

    let friendsInRequest   = 20
    var firstTimeAppear    = true
    var userAutorize       = false
    var friends            = [VGFriend]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if firstTimeAppear {
            VGDataManager.sharedManager.autorizeUser(success: { (result) in
                self.userAutorize    = result
                self.firstTimeAppear = false
            })
        }

        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(refreshFriends), for: .valueChanged)
        self.refreshControl = refresh
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if userAutorize {
            self.getFriends()
            self.userAutorize = false
        }
    }

    
    //MARK: - UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friends.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == friends.count - 1 {
            self.getFriends()
        }
        
        let identifier = "Cell"
        let friend     = friends[indexPath.row]
        let cell       = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)

        cell.textLabel?.text  = "\(friend.firstName) \(friend.lastName)"
        cell.imageView?.image = nil
        
        if let url = friend.imageURL {
            VGDataManager.sharedManager.getImageOfFriend(imageURL: url, success: { (image) in
                
                cell.imageView?.image = image
                cell.layoutSubviews()
                cell.imageView?.layer.cornerRadius  = (cell.imageView?.bounds.width)! / 2
                cell.imageView?.layer.masksToBounds = true
                
            }) { (error) in
                print("Get image Error: \(error?.localizedDescription)")
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            let friend = friends[indexPath.row]
            
            VGDataManager.sharedManager.deleteFriend(userID: friend.id, success: { (success) in
                if success {
                    
                    do {
                        try realm.write {
                            realm.delete(self.friends[indexPath.row])
                        }
                    } catch {
                        print("Can't delete friend from REALM!!!")
                    }
                    
                    self.friends.remove(at: indexPath.row)
                    
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    
                } else {
                    print("Can't delete friend from table view!!!")
                }
            })
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    
    //MARK - ServerMethods
    func getFriends() {
        
        if let userID = VGDataManager.sharedManager.currentAccessToken.userID {
            
            VGDataManager.sharedManager.getFriendsFromServer(ID: userID, count: friendsInRequest, offset: friends.count, success: { (arrayOfFriends) in
                
                self.friends.append(contentsOf: arrayOfFriends)
                
                var arrayOfIndexPath = [IndexPath]()
                
                for i in (self.friends.count - arrayOfFriends.count)..<self.friends.count {
                    arrayOfIndexPath.append(IndexPath(row: i, section: 0))
                }
                
                self.tableView.beginUpdates()
                self.tableView.insertRows(at: arrayOfIndexPath, with: .left)
                self.tableView.endUpdates()
                
            }) { (error) in
                print("Get friends Error: \(error?.localizedDescription)")
            }
        }
    }
    
    
    //MARK: - MyMethods
    
    func refreshFriends() {
        
        self.friends.removeAll()
        self.tableView.reloadData()
        self.getFriends()
        self.refreshControl?.endRefreshing()
    }
    
    
    //MARK: - Actions
    @IBAction func addNewFriendAction(_ sender: UIButton) {
        
        let newFriendVC = storyboard?.instantiateViewController(withIdentifier: "NewFriendViewController") as! NewFriendViewController
        self.navigationController?.pushViewController(newFriendVC, animated: true)
    }
    
    @IBAction func editAction(_ sender: UIButton) {
        
        let isEditing = self.tableView.isEditing
        self.tableView.setEditing(!isEditing, animated: true)
        
        if self.tableView.isEditing {
            sender.setTitle("Done", for: .normal)
        } else {
            sender.setTitle("Edit", for: .normal)
        }
    }
    

}
