//
//  AutorizeViewController.swift
//  RealmAlamofireTest
//
//  Created by Admin on 13.03.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import Foundation

class AutorizeViewController: UIViewController, UIWebViewDelegate {
    
    var webView : UIWebView? = nil
    var clouser : ((VGAccessToken) -> ())? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let webView = UIWebView(frame: self.view.bounds)
        webView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        webView.delegate = self
        
        self.webView = webView
        self.view.addSubview(webView)
        self.navigationItem.title = "Autorization"
        
        let urlString = "https://oauth.vk.com/authorize?" + "client_id=5919055&" + "display=mobile&" + "redirect_uri=https://oauth.vk.com/blank.html&" + "scope=271390&" + "response_type=token&v=5.62"
        let url = URL(string: urlString)
        let request = URLRequest(url: url!)
        
        webView.loadRequest(request)
    }
    
    deinit {
        self.webView?.delegate = nil
    }
    
    
    //MARK: - UIWebViewDelegate
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if (request.url?.description.hasPrefix("https://oauth.vk.com/blank.html"))! {
            
            let accessToken = VGAccessToken()
            var query       = request.url?.description
            let array       = query?.components(separatedBy: "#")
            
            if let myArray = array, myArray.count > 1 {
                query = myArray.last
            }
            
            let pairs = query?.components(separatedBy: "&")
            
            if let myPairs = pairs {
                for pair in myPairs {
                    
                    let values = pair.components(separatedBy: "=")
                    
                    if values.count == 2 {
                        switch values.first! {
                            case "access_token": accessToken.token = values.last
                            case "expires_in"  : accessToken.expirationDate = NSDate(timeIntervalSinceNow: Double(values.last!) ?? 0)
                            case "user_id"     : accessToken.userID = Int(values.last!)
                            default: break
                        }
                    }
                }
            }

            self.webView?.delegate = nil
            
            if self.clouser != nil {
                self.clouser!(accessToken)
            }
            
            self.dismiss(animated: true, completion: nil)
            
            return false
        }
        return true
    }
}
