//
//  NewFriendViewController.swift
//  RealmAlamofireTest
//
//  Created by Admin on 06.03.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit

class NewFriendViewController: UIViewController {

    @IBOutlet weak var friendIDTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: - Actions
    @IBAction func addFriendAction(_ sender: UIButton) {
        
        if let text = friendIDTextField.text {
            
            VGDataManager.sharedManager.addFriend(userID: Int(text), success: { (result) in
                print(result)
            }, failure: { (error) in
                print("Add friend error: \(error?.localizedDescription)")
            })
            
            let _ = self.navigationController?.popViewController(animated: true)
        }
    }
}
