//
//  SubscriptionsViewController.swift
//  APITestOnSwift
//
//  Created by Admin on 07.02.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import Alamofire

class SubscriptionsViewController: UITableViewController {
    
    var friendID : Int!
    var subscriptions = [User]()
    var arrayOfID     = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Subscriptions"
        self.getSubscriptorsFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: - APIMethods
    
    func getSubscriptorsFromServer() {
        
        ServerManager.sharedManager.getSubscriptions(id: self.friendID, success: { (arrayOfID) in
            self.arrayOfID = arrayOfID
            
            ServerManager.sharedManager.getUsersSubscription(ids: self.arrayOfID, success: { (users) in
                self.subscriptions = users
                self.tableView.reloadData()
            }, failure: { (error) in
                print("Get array of users subscription error: \(error?.localizedDescription)")
            })
        }) { (error) in
            print("Get array of ID error: \(error?.localizedDescription)")
        }
    }

    
    //MARK: - UITableViewDataSource

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.subscriptions.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "SubscriptionCell"
        let user = self.subscriptions[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier)

        weak var weakCell = cell
        
        cell?.textLabel?.text  = user.fullName
        cell?.imageView?.image = nil
        
        if let url = user.imageURL {
            ServerManager.sharedManager.getImage(url: url, success: { (image) in
                
                weakCell?.imageView?.image = image
                weakCell?.layoutSubviews()
                weakCell?.imageView?.layer.cornerRadius = (weakCell?.imageView?.bounds.width)! / 2
                weakCell?.imageView?.layer.masksToBounds = true
                
            }) { (error) in
                print("Image for subscription error: \(error?.localizedDescription)")
            }
        }
        return cell!
    }
    
    
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
}
