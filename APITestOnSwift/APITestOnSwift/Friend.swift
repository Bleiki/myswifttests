//
//  Friend.swift
//  APITestOnSwift
//
//  Created by Admin on 05.02.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation
import Alamofire

class Friend {
    
    let response    : [String : Any]
    var friendVC    : FriendViewController! //Необходимо для установки полей страны и города
    var imageURL    : URL!
    var friendID    = Int()
    var firstName   = String()
    var lastName    = String()
    var cityName    = String()
    var countryName = String()
    var cityID      = Int()
    var countryID   = Int()
    var online      = String()
    var fullName    : String {
        return firstName + " " + lastName
    }
    
    init(response: [String : Any]) {
       self.response = response
    }
    
    func setParams() {
        
        self.firstName = (response["first_name"] as? String) ?? ""
        self.lastName  = (response["last_name"] as? String) ?? ""
        self.friendID    = (response["uid"] as? Int) ?? 0
        self.cityID    = (response["city"] as? Int) ?? 0
        self.countryID = (response["country"] as? Int) ?? 0
        
        if (response["online"] as? Bool)! {
            self.online = "online"
        } else {
            self.online = "not online"
        }
        
        if let string  = (response["photo_max"] as? String) {
            self.imageURL  = URL(string: string)!
        }

        ServerManager.sharedManager.getCountry(id: self.countryID, success: { (name) in
            self.countryName = name
            self.friendVC.countryCell.detailTextLabel?.text = name
        }) { (error) in
            print("Country get error: \(error?.localizedDescription)")
            self.countryName = ""
            self.friendVC.countryCell.detailTextLabel?.text = self.countryName
        }
        
        ServerManager.sharedManager.getCity(id: cityID, success: { (name) in
            self.cityName = name
            self.friendVC.cityCell.detailTextLabel?.text = name
        }) { (error) in
            print("City get error: \(error?.localizedDescription)")
            self.cityName = ""
            self.friendVC.cityCell.detailTextLabel?.text = self.cityName
        }
    }
}
