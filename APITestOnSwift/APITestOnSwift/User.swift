//
//  User.swift
//  APITestOnSwift
//
//  Created by Admin on 01.02.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation

class User {
    
    var userID    : Int
    var firstName : String
    var lastName  : String
    var imageURL  : URL?
    var fullName  : String {
        return firstName + " " + lastName
    }

    //Создаю юзера на основе пришедших данных
    init(response: [String : Any]) {
        
        self.firstName = (response["first_name"] as? String) ?? ""
        self.lastName  = (response["last_name"] as? String) ?? ""
        self.userID    = (response["uid"] as? Int)!
        if let string  = (response["photo_50"] as? String) {
            self.imageURL  = URL(string: string)!
        }
    }
}
