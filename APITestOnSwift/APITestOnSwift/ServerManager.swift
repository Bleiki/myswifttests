//
//  ServerManager.swift
//  APITestOnSwift
//
//  Created by Admin on 04.02.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class ServerManager {
    
    static let sharedManager = ServerManager()

    //Функция для получения результатов запроса(в клоужер передается массив Юзеров или ошибка в зависимости от результата) 
    //@escaping означает что сначала выполнется функция, а потом этот клоужер
    func getFriendsWith(offset: Int, count: Int, success: @escaping(_ friends: [User]) -> (), failure: @escaping(_ error: Error?) -> ()) {
        
        //Адрес по которому делается запрос
        let url = "https://api.vk.com/method/friends.get"
        
        //Параметры для запроса
        let params : [String : Any] = [
            "user_id"   : 49369760,
            "order"     : "name",
            "count"     : count,
            "offset"    : offset,
            "fields"    : "photo_50",
            "name_case" : "nom"]
        
        //Сам запрос (для работы надо указывать не просто .default, а так как указано здесь)
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (response) in
            
            //В случае успеха
            if response.result.isSuccess {
                print(response)
                //В случае, если данные пришли (не пустые)
                if let arrayOfDict = (response.result.value as? [String : Any])?["response"] as? [[String : Any]], !arrayOfDict.isEmpty {
                    
                    var friendsArray = [User]()
                    
                    for object in arrayOfDict {
                        let friend = User.init(response: object)
                        friendsArray.append(friend)
                    }
                    
                    success(friendsArray)
                //Если данные пусты
                } else {
                    failure(nil)
                }
            //В случае какой либо ошибки
            } else if response.result.isFailure {
                failure(response.result.error)
            }
        }
    }
    
    func getFriend(id: Int, success: @escaping(_ friend: Friend) -> (), failure: @escaping(_ error: Error?) -> ()) {
        
        let url = "https://api.vk.com/method/users.get"
        let params : [String : Any] = [
        "user_ids"  : id,
        "fields"    : "city, country, online, photo_max",
        "name_case" : "nom"]
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (response) in
            
            if response.result.isSuccess {
                print(response)
                
                if let arrayOfParams = (response.result.value as? [String : Any])?["response"] as? [[String : Any]], !arrayOfParams.isEmpty {
                    
                    let friend = Friend.init(response: arrayOfParams[0])
                    friend.setParams()//Для задания параметров
                    success(friend)
                    
                } else {
                    failure(nil)
                }
                
            } else if response.result.isFailure {
                failure(response.result.error)
            }
        }
    }
    
    func getUsersSubscription(ids: [Int], success: @escaping(_ users: [User]) -> (), failure: @escaping(_ error: Error?) -> ()) {
        
        let url = "https://api.vk.com/method/users.get"
        let params : [String : Any] = [
            "user_ids"  : ids,
            "fields"    : "photo_50",
            "name_case" : "nom"]
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (response) in
            
            if response.result.isSuccess {
                print(response)
                
                if let arrayOfParams = (response.result.value as? [String : Any])?["response"] as? [[String : Any]], !arrayOfParams.isEmpty {
                    
                    var users = [User]()
                    
                    for i in 0..<arrayOfParams.count {
                        let user = User.init(response: arrayOfParams[i])
                        users.append(user)
                    }
                    success(users)
                    
                } else {
                    failure(nil)
                }
                
            } else if response.result.isFailure {
                failure(response.result.error)
            }
        }
    }
    
    func getFollowers(id: Int, offset: Int, count: Int, success: @escaping(_ followers: [User]) -> (), failure: @escaping(_ error: Error?) -> ()) {
        
        let url = "https://api.vk.com/method/users.getFollowers"
        let params : [String : Any] = [
            "user_id"   : id,
            "offset"    : offset,
            "count"     : count,
            "fields"    : "photo_50",
            "name_case" : "nom"]
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (response) in
            
            if response.result.isSuccess {
                print(response)
                
                if let arrayOfDict = ((response.result.value as? [String : Any])?["response"] as? [String : Any])?["items"] as? [[String : Any]], !arrayOfDict.isEmpty {
                    
                    var followersArray = [User]()
                    
                    for object in arrayOfDict {
                        let follower = User.init(response: object)
                        followersArray.append(follower)
                    }
                    success(followersArray)
                    
                } else {
                    failure(nil)
                }
                
            } else if response.result.isFailure {
                failure(response.result.error)
            }
        }
    }
    
    func getSubscriptions(id: Int, success: @escaping(_ subscriptions: [Int]) -> (), failure: @escaping(_ error: Error?) -> ()) {
        
        let url = "https://api.vk.com/method/users.getSubscriptions"
        let params : [String : Any] = ["user_id"   : id]
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (response) in
            
            if response.result.isSuccess {
                print(response)
                
                if let arrayOfID = (((response.result.value as? [String : Any])?["response"] as? [String : Any])?["users"] as? [String : Any])?["items"] as? [Int], !arrayOfID.isEmpty {
                        success(arrayOfID)
                } else {
                    failure(nil)
                }
                
            } else if response.result.isFailure {
                failure(response.result.error)
            }
        }
    }
   
    func getCity(id: Int, success: @escaping(_ cityName: String) -> (), failure: @escaping(_ error: Error?) -> ()) {
        
        let url = "https://api.vk.com/method/database.getCitiesById"
        let params : [String : Any] = ["city_ids" : id]
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (response) in
            
            if response.result.isSuccess {
                print(response)
                
                if let params = (response.result.value as? [String : Any])?["response"] as? [[String : Any]], !params.isEmpty {
                    let cityName = params[0]["name"] as! String
                    success(cityName)
                } else {
                    failure(nil)
                }
            } else if response.result.isFailure {
                failure(response.result.error)
            }
        }
    }
    
    func getCountry(id: Int, success: @escaping(_ countryName: String) -> (), failure: @escaping(_ error: Error?) -> ()) {
        
        let url = "https://api.vk.com/method/database.getCountriesById"
        let params : [String : Any] = ["country_ids" : id]
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (response) in
            
            if response.result.isSuccess {
                print(response)
                
                if let params = (response.result.value as? [String : Any])?["response"] as? [[String : Any]], !params.isEmpty {
                    let countryName = params[0]["name"] as! String
                    success(countryName)
                } else {
                    failure(nil)
                }
            } else if response.result.isFailure {
                failure(response.result.error)
            }
        }
    }
    
    //Получение данных НЕ в виде JSON
    func getImage(url: URL, success: @escaping(_ image: UIImage) -> (), failure: @escaping(_ error: Error?) -> ()) {
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).response { (response) in

            if let data = response.data {
                let image = UIImage(data: data) ?? UIImage()
                success(image)
            } else {
                failure(nil)
            }
        }
    }
}
