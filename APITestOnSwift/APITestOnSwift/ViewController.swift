//
//  ViewController.swift
//  APITestOnSwift
//
//  Created by Admin on 01.02.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import AlamofireImage

class ViewController: UITableViewController {
    
    var myFriends        = [User]()
    let friendsInRequest = 20
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.getFriendsFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - APIMethods
    //Заполняю массив друзей и вставляю анимированно новые ячейки при запросе новых друзей
    func getFriendsFromServer() {
        
        ServerManager.sharedManager.getFriendsWith(offset: myFriends.count, count: friendsInRequest, success: { (friendsArray) in
            
            self.myFriends.append(contentsOf: friendsArray)
            
            var arrayOfIndexPath = [IndexPath]()
            
            for i in (self.myFriends.count - friendsArray.count)..<self.myFriends.count {
                arrayOfIndexPath.append(IndexPath.init(row: i, section: 0))
            }
            
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: arrayOfIndexPath, with:  UITableViewRowAnimation.top)
            self.tableView.endUpdates()
            
        }) { (error) in
            print("Friends list error: \(error?.localizedDescription)")
        }
    }

    
    //MARK: - UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myFriends.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Когда прорисуется последняя ячейка, будет выполнен новый запрос
        if indexPath.row == self.myFriends.count - 1 {
            self.getFriendsFromServer()
        }
        
        let identifier    = "Cell"
        let friend        = self.myFriends[indexPath.row]
        let cell          = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        weak var weakCell = cell

        cell?.textLabel?.text  = friend.fullName
        cell?.imageView?.image = nil //Установка картинки в нил, чтобы она не прыгала
        
        if let url = friend.imageURL {
            ServerManager.sharedManager.getImage(url: url, success: { (image) in

                    weakCell?.imageView?.image = image
                    weakCell?.layoutSubviews() //Перерисовка вьюхи
                    weakCell?.imageView?.layer.cornerRadius = (weakCell?.imageView?.bounds.width)! / 2 //Округление фото
                    weakCell?.imageView?.layer.masksToBounds = true //Разрешение изменять слой
                
            }) { (error) in
                print("Image for friend error: \(error?.localizedDescription)")
            }
        }
        return cell!
    }
    
    
    //MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)

        //Запуск контроллера из сториборада(Identifier контроллеру устанавливается в Storyboard ID)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FriendViewController") as! FriendViewController
        vc.friendID = self.myFriends[indexPath.row].userID
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

