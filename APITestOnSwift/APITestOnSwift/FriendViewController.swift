//
//  FriendViewController.swift
//  APITestOnSwift
//
//  Created by Admin on 05.02.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import Foundation

class FriendViewController: UITableViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var onlineLabel: UILabel!
    @IBOutlet weak var firstNameCell: UITableViewCell!
    @IBOutlet weak var lastNameCell: UITableViewCell!
    @IBOutlet weak var countryCell: UITableViewCell!
    @IBOutlet weak var cityCell: UITableViewCell!

    let followersCellNumber = 6
    let subscriptionsCellNumber = 7
    var friendID : Int!
    var friend   : Friend!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Запрос на получение информации о друге по айди
        ServerManager.sharedManager.getFriend(id: self.friendID, success: { (friend) in

            self.friend = friend
            self.friend.friendVC = self
            
            self.navigationItem.title = self.friend.fullName
            //Не устанавливаю self.countryCell.detailTextLabel?.text и self.cityCell.detailTextLabel?.text, т.к. данные о городе и стране будут получены лиь после завершения viewDidLoad()
            self.firstNameCell.textLabel?.text       = "First name"
            self.firstNameCell.detailTextLabel?.text = self.friend.firstName
            self.lastNameCell.textLabel?.text        = "Last name"
            self.lastNameCell.detailTextLabel?.text  = self.friend.lastName
            self.countryCell.textLabel?.text         = "Country"
            self.cityCell.textLabel?.text            = "City"
            self.onlineLabel.text                    = self.friend.online
            
            if let url = self.friend.imageURL {
                ServerManager.sharedManager.getImage(url: url, success: { (image) in
                    
                    self.imageView.image = image
                    self.imageView.layer.cornerRadius = (self.imageView.bounds.width) / 2
                    self.imageView.layer.masksToBounds = true
                    self.tableView.layoutSubviews()
                    
                }) { (error) in
                    print("Image for friend error: \(error?.localizedDescription)")
                }
            }
        }) { (error) in
            print("Friend params error: \(error?.localizedDescription)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == self.followersCellNumber {
            
            let followersVC = self.storyboard?.instantiateViewController(withIdentifier: "FollowersViewController") as! FollowersViewController
            followersVC.friendID = self.friend.friendID
            self.navigationController?.pushViewController(followersVC, animated: true)
            
        } else if indexPath.row == self.subscriptionsCellNumber {
            
            let subscriptionsVC = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionsViewController") as! SubscriptionsViewController
            subscriptionsVC.friendID = self.friend.friendID
            self.navigationController?.pushViewController(subscriptionsVC, animated: true)
        }
    }
}
