//
//  FollowersViewController.swift
//  APITestOnSwift
//
//  Created by Admin on 07.02.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit

class FollowersViewController: UITableViewController {
    
    var friendID  : Int!
    var followers = [User]()
    let followersInRequest = 20

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Followers"
        self.getFollowersFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: - APIMethods
    //Заполняю массив подписчиков и вставляю анимированно новые ячейки при запросе новых подписчиков
    func getFollowersFromServer() {
        
        ServerManager.sharedManager.getFollowers(id: friendID, offset: followers.count, count: followersInRequest, success: { (followersFromRequest) in
            
            
            self.followers.append(contentsOf: followersFromRequest)
            
            var arrayOfIndexPath = [IndexPath]()
            
            for i in (self.followers.count - followersFromRequest.count)..<self.followers.count {
                arrayOfIndexPath.append(IndexPath.init(row: i, section: 0))
            }
            
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: arrayOfIndexPath, with:  UITableViewRowAnimation.top)
            self.tableView.endUpdates()
            
        }) { (error) in
            print("Followers list error: \(error?.localizedDescription)")
        }
    }
    

    // MARK: - UITableViewDataSource

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.followers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == self.followers.count - 1 {
            self.getFollowersFromServer()
        }
        
        let identifier = "FollowerCell"
        let follower   = self.followers[indexPath.row]
        let cell       = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        weak var weakCell = cell
        
        cell?.textLabel?.text  = follower.fullName
        cell?.imageView?.image = nil //Установка картинки в нил, чтобы она не прыгала
        
        if let url = follower.imageURL {
            ServerManager.sharedManager.getImage(url: url, success: { (image) in
                
                weakCell?.imageView?.image = image
                weakCell?.layoutSubviews() //Перерисовка вьюхи
                weakCell?.imageView?.layer.cornerRadius = (weakCell?.imageView?.bounds.width)! / 2 //Округление фото
                weakCell?.imageView?.layer.masksToBounds = true //Разрешение изменять слой
                
            }) { (error) in
                print("Image for follower error: \(error?.localizedDescription)")
            }
        }
        return cell!
    }

    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
}
