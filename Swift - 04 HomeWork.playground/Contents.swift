
let string1 = "1"
let string2 = "3rt2"
let string3 = "7"
let string4 = "124arf"
let string5 = "12"

var result = 0

if let number1 = Int(string1) {
    result += number1
} else {
    print("String1 is not number!")
}

if let number2 = Int(string2) {
    result += number2
} else {
    print("String2 is not number!")
}

if let number3 = Int(string3) {
    result += number3
} else {
    print("String3 is not number!")
}

if Int(string4) == nil {
    print("String4 is not number!")
} else {
    let number4 = Int(string4)
    result += number4!
}

if Int(string5) == nil {
    print("String5 is not number!")
} else {
    let number5 = Int(string5)
    result += number5!
}

print(result)


let tupleFromServer : (statusCode : Int, message : String?, errorMessage : String?) = (254, "Hi, i'm server", "Hi, i'm error")

if tupleFromServer.statusCode >= 200 && tupleFromServer.statusCode < 300 {
    
    if tupleFromServer.message != nil {
        print(tupleFromServer.message!)
    } else {
        print("Message is nil")
    }
    
} else {
    
    if tupleFromServer.errorMessage != nil {
        print(tupleFromServer.errorMessage!)
    } else {
        print("Error message is nil")
    }
}


let student1 : (name : String, numberOfCar : String?, evaluation : Int?)
let student2 : (name : String, numberOfCar : String?, evaluation : Int?)
let student3 : (name : String, numberOfCar : String?, evaluation : Int?)
let student4 : (name : String, numberOfCar : String?, evaluation : Int?)
let student5 : (name : String, numberOfCar : String?, evaluation : Int?)

student1.name = "Jon"
student2.name = "Mark"
student3.name = "Stefani"
student4.name = "Kristi"
student5.name = "Polli"

student1.numberOfCar = "jk123o"
student2.numberOfCar = nil
student3.numberOfCar = "oi435b"
student4.numberOfCar = nil
student5.numberOfCar = nil

student1.evaluation = 3
student2.evaluation = 4
student3.evaluation = 2
student4.evaluation = 5
student5.evaluation = nil

if let myNumberOfCar = student1.numberOfCar, let myEvaluation = student1.evaluation {
    
    print("Name: \(student1.name)\n" +
        "Have a car: YES\n" +
        "Number of the car: \(myNumberOfCar)\n" +
        "Visited the control: YES\n" +
        "Evaluation: \(myEvaluation)\n")
    
} else if student1.numberOfCar == nil && student1.evaluation == nil {
    
    print("Name: \(student1.name)\n" +
        "Have a car: NO\n" +
        "Visited the control: NO\n")
    
} else if let myNumberOfCar = student1.numberOfCar, student1.evaluation == nil {
    
    print("Name: \(student1.name)\n" +
        "Have a car: YES\n" +
        "Number of the car: \(myNumberOfCar)\n" +
        "Visited the control: NO\n")
    
} else if student1.numberOfCar == nil, let myEvaluation = student1.evaluation  {
    
    print("Name: \(student1.name)\n" +
        "Have a car: NO\n" +
        "Visited the control: YES\n" +
        "Evaluation: \(myEvaluation)\n")
}


if let myNumberOfCar = student2.numberOfCar, let myEvaluation = student2.evaluation {
    
    print("Name: \(student2.name)\n" +
        "Have a car: YES\n" +
        "Number of the car: \(myNumberOfCar)\n" +
        "Visited the control: YES\n" +
        "Evaluation: \(myEvaluation)\n")
    
} else if student2.numberOfCar == nil && student2.evaluation == nil {
    
    print("Name: \(student2.name)\n" +
        "Have a car: NO\n" +
        "Visited the control: NO\n")
    
} else if let myNumberOfCar = student2.numberOfCar, student2.evaluation == nil {
    
    print("Name: \(student2.name)\n" +
        "Have a car: YES\n" +
        "Number of the car: \(myNumberOfCar)\n" +
        "Visited the control: NO\n")
    
} else if student2.numberOfCar == nil, let myEvaluation = student2.evaluation  {
    
    print("Name: \(student2.name)\n" +
        "Have a car: NO\n" +
        "Visited the control: YES\n" +
        "Evaluation: \(myEvaluation)\n")
}


if let myNumberOfCar = student3.numberOfCar, let myEvaluation = student3.evaluation {
    
    print("Name: \(student3.name)\n" +
        "Have a car: YES\n" +
        "Number of the car: \(myNumberOfCar)\n" +
        "Visited the control: YES\n" +
        "Evaluation: \(myEvaluation)\n")
    
} else if student3.numberOfCar == nil && student3.evaluation == nil {
    
    print("Name: \(student3.name)\n" +
        "Have a car: NO\n" +
        "Visited the control: NO\n")
    
} else if let myNumberOfCar = student3.numberOfCar, student3.evaluation == nil {
    
    print("Name: \(student3.name)\n" +
        "Have a car: YES\n" +
        "Number of the car: \(myNumberOfCar)\n" +
        "Visited the control: NO\n")
    
} else if student3.numberOfCar == nil, let myEvaluation = student3.evaluation  {
    
    print("Name: \(student3.name)\n" +
        "Have a car: NO\n" +
        "Visited the control: YES\n" +
        "Evaluation: \(myEvaluation)\n")
}


if let myNumberOfCar = student4.numberOfCar, let myEvaluation = student4.evaluation {
    
    print("Name: \(student4.name)\n" +
        "Have a car: YES\n" +
        "Number of the car: \(myNumberOfCar)\n" +
        "Visited the control: YES\n" +
        "Evaluation: \(myEvaluation)\n")
    
} else if student4.numberOfCar == nil && student4.evaluation == nil {
    
    print("Name: \(student4.name)\n" +
        "Have a car: NO\n" +
        "Visited the control: NO\n")
    
} else if let myNumberOfCar = student4.numberOfCar, student4.evaluation == nil {
    
    print("Name: \(student4.name)\n" +
        "Have a car: YES\n" +
        "Number of the car: \(myNumberOfCar)\n" +
        "Visited the control: NO\n")
    
} else if student4.numberOfCar == nil, let myEvaluation = student4.evaluation  {
    
    print("Name: \(student4.name)\n" +
        "Have a car: NO\n" +
        "Visited the control: YES\n" +
        "Evaluation: \(myEvaluation)\n")
}


if let myNumberOfCar = student5.numberOfCar, let myEvaluation = student5.evaluation {
    
    print("Name: \(student5.name)\n" +
        "Have a car: YES\n" +
        "Number of the car: \(myNumberOfCar)\n" +
        "Visited the control: YES\n" +
        "Evaluation: \(myEvaluation)\n")
    
} else if student5.numberOfCar == nil && student5.evaluation == nil {
    
    print("Name: \(student5.name)\n" +
        "Have a car: NO\n" +
        "Visited the control: NO\n")
    
} else if let myNumberOfCar = student5.numberOfCar, student5.evaluation == nil {
    
    print("Name: \(student5.name)\n" +
        "Have a car: YES\n" +
        "Number of the car: \(myNumberOfCar)\n" +
        "Visited the control: NO\n")
    
} else if student5.numberOfCar == nil, let myEvaluation = student5.evaluation  {
    
    print("Name: \(student5.name)\n" +
        "Have a car: NO\n" +
        "Visited the control: YES\n" +
        "Evaluation: \(myEvaluation)\n")
}






















