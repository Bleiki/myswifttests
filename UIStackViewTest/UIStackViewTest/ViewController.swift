//
//  ViewController.swift
//  UIStackViewTest
//
//  Created by Admin on 01.03.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit

//@available (iOS 9, *)

class ViewController: UIViewController {

    @IBOutlet weak var verticalStackView: UIStackView!
    @IBOutlet weak var horizontalStackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Methods
    func randomColor() -> UIColor {
        
        let r = CGFloat((arc4random() % 256)) / 255.0
        let g = CGFloat((arc4random() % 256)) / 255.0
        let b = CGFloat((arc4random() % 256)) / 255.0
        
        return UIColor(red: r, green: g, blue: b, alpha: 1)
    }

    //MARK: - Actions
    @IBAction func addNewButtonAction(_ sender: UIButton) {
        
        let button = UIButton(type: .system)
        button.setTitle("NEW", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = randomColor()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 40, weight: UIFontWeightLight)
        button.isHidden = true
        button.addTarget(self, action: #selector(removeButtonAction(_:)), for: .touchUpInside)
        
        self.verticalStackView.insertArrangedSubview(button, at: 1)
        
        UIView.animate(withDuration: 0.5) { 
            button.isHidden = false
        }
    }
    
    @IBAction func removeButtonAction(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, animations: {
            sender.alpha = 0
            sender.isHidden = true
        })
    }
    
    @IBAction func addImageButtonAction(_ sender: UIButton) {
        
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.backgroundColor = randomColor()
        
        self.horizontalStackView.addArrangedSubview(image)
        
        UIView.animate(withDuration: 0.25) { 
            self.horizontalStackView.layoutIfNeeded()
        }
    }
    
    @IBAction func removeImageButtonAction(_ sender: UIButton) {
        
        let image = self.horizontalStackView.arrangedSubviews.last
        
        guard let myImage = image else {
            print("Comming NIL")
            return
        }
        
        self.horizontalStackView.removeArrangedSubview(myImage)
        image?.removeFromSuperview()
        
        UIView.animate(withDuration: 0.25) { 
            self.horizontalStackView.layoutIfNeeded()
        }
    }

}

