
let myMaxNumberOfRepetitions = (pushUps:70, pullUps:18, squats:80)

print("My stats:\n\(myMaxNumberOfRepetitions)")
print("My push ups: \(myMaxNumberOfRepetitions.pushUps)")
print("My pull ups: \(myMaxNumberOfRepetitions.1)")
print("My squats: \(myMaxNumberOfRepetitions.2)")

var myFriendMaxNumberOfRepetitions = myMaxNumberOfRepetitions

myFriendMaxNumberOfRepetitions.pushUps = 55
myFriendMaxNumberOfRepetitions.pullUps = 23
myFriendMaxNumberOfRepetitions.squats = 75

print("My friend stats:\n\(myFriendMaxNumberOfRepetitions)")
print("My friend push ups: \(myFriendMaxNumberOfRepetitions.pushUps)")
print("My friend pull ups: \(myFriendMaxNumberOfRepetitions.1)")
print("My friend squats: \(myFriendMaxNumberOfRepetitions.squats)")

var differenceRepetitions = myMaxNumberOfRepetitions

differenceRepetitions.pushUps = myMaxNumberOfRepetitions.pushUps - myFriendMaxNumberOfRepetitions.pushUps
differenceRepetitions.pullUps = myMaxNumberOfRepetitions.pullUps - myFriendMaxNumberOfRepetitions.pullUps
differenceRepetitions.squats = myMaxNumberOfRepetitions.squats - myFriendMaxNumberOfRepetitions.squats

print("Difference in stats:\n\(differenceRepetitions)")
print("Difference in push ups: \(differenceRepetitions.pushUps)")
print("Difference in pull ups: \(differenceRepetitions.1)")
print("Difference in squats: \(differenceRepetitions.squats)")
