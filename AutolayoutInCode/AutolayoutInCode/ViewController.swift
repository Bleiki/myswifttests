//
//  ViewController.swift
//  AutolayoutInCode
//
//  Created by Admin on 26.02.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var view1 : UIView!
    var view2 : UIView!
    var view3 : UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        createConstaints()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func initViews() {
        
        self.view1 = UIView()
        self.view1.backgroundColor = UIColor.lightGray
        self.view1.translatesAutoresizingMaskIntoConstraints = false//Обязательно ставить
        self.view.addSubview(view1)
        
        self.view2 = UIView()
        self.view2.backgroundColor = UIColor.darkGray
        self.view2.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(view2)
        
        self.view3 = UIView()
        self.view3.backgroundColor = UIColor.red
        self.view3.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(view3)
    }
    
    func createConstaints() {
        
        //For view1
        //Если отсутствует второй айтем к которому идет привязка, то вместо айтема надо передать nil, а в атрибут .nonAnAttribute
        let pinLeftView1 = NSLayoutConstraint(item: self.view1, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1.0, constant: 0)//Левая сторона view1 д.б. расположена на 0 пикселей правее левой стороны self.view.
        
        let pinTopView1 = NSLayoutConstraint(item: self.view1, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0)
        
        let pinRightView1 = NSLayoutConstraint(item: self.view1, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: 0)
        
        let verticalView1 = NSLayoutConstraint(item: self.view1, attribute: .bottom, relatedBy: .equal, toItem: self.view2, attribute: .top, multiplier: 1.0, constant: 0)
        
        let heightView1 = NSLayoutConstraint(item: self.view1, attribute: .height, relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 0.65, constant: 0)
        
        //For View2
        let pinLeftView2 = NSLayoutConstraint(item: self.view2, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1.0, constant: 0)
        
        let pinBottomView2 = NSLayoutConstraint(item: self.view2, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0)
        
        let pinRightView2 = NSLayoutConstraint(item: self.view2, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: 0)
        
        //For View3
        let centerXImage = NSLayoutConstraint(item: self.view3, attribute: .centerX, relatedBy: .equal, toItem: self.view1, attribute: .centerX, multiplier: 1.0, constant: 0)
        
        let centerYImage = NSLayoutConstraint(item: self.view3, attribute: .centerY, relatedBy: .equal, toItem: self.view1, attribute: .centerY, multiplier: 1.0, constant: 0)
        
        let widthImage = NSLayoutConstraint(item: self.view3, attribute: .width, relatedBy: .equal, toItem: self.view3, attribute: .width, multiplier: 0.0, constant: 200)
        
        let heightImage = NSLayoutConstraint(item: self.view3, attribute: .height, relatedBy: .equal, toItem: self.view3, attribute: .height, multiplier: 0.0, constant: 200)
        
        //После создания ограничений, их надо добавить к соответствующему виду, в данном случае к self.view
        self.view.addConstraints([pinLeftView1, pinTopView1, pinRightView1, verticalView1, heightView1, pinLeftView2, pinBottomView2, pinRightView2, centerXImage, centerYImage, widthImage, heightImage])
    }
}

