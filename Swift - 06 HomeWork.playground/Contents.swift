

let str1 = "1af"
let str2 = "12"
let str3 = "4"
let str4 = "1a1f"
let str5 = "13"
let str6 = "31"
let str7 = "124"

var sum = 0

sum += Int(str1) ?? 0
sum += Int(str2) ?? 0
sum += Int(str3) ?? 0
sum += Int(str4) ?? 0
sum += Int(str5) ?? 0
sum += Int(str6) ?? 0
sum += Int(str7) ?? 0

let resultStr = "\(Int(str1) ?? 0)" + " + " + str2 + " + " + str3 + " + " + str4 + " + " + str5 + " + " + str6 + " + " + str7 + " + " + " = " + "\(sum)"

//print(resultStr)

let bigString = "abcdefghijklmnoqprstuwxyz"

let character : Character = "g"

for (index, char) in bigString.characters.enumerated() {
    if char == character {
        print("Index: \(index)")
    }
}


