//
//  CourseInfoViewController.swift
//  CoreDataHW
//
//  Created by Admin on 28.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class CourseInfoViewController: UITableViewController {

    var course : Course?
    let attributesOfCourse = 3

    init() {
        super.init(style: .plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = course!.name ?? "No name"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section {
        case 0:
            return "Course info"
        default:
            return "Students"
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return attributesOfCourse
        default:
            return course?.students?.count ?? 0
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let infoCell = UITableViewCell(style: .value1, reuseIdentifier: "InfoCell")
            
            if indexPath.row == 0 {
                infoCell.textLabel!.text = "Course name"
                infoCell.detailTextLabel!.text = course?.name ?? "No first name"
            } else if indexPath.row == 1 {
                infoCell.textLabel!.text = "Direction"
                infoCell.detailTextLabel!.text = course?.direction ?? "No direction"
            } else {
                infoCell.textLabel!.text = "Teacher"
                infoCell.detailTextLabel!.text = ((course?.teacher?.firstName!)! + " " + (course?.teacher?.lastName!)!) 
            }
            return infoCell
            
        default:
            let studentCell = UITableViewCell(style: .default, reuseIdentifier: "StudentCell")

            if course!.students != nil {
                let student = self.selectedStudent(students: course!.students!, indexPath: indexPath)
                
                studentCell.textLabel!.text = (student.firstName ?? "No firstName") + " " + (student.lastName ?? "No lastName")
            }
            return studentCell
        }
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    // MARK: - MyMethods
    
    func selectedStudent(students: NSSet, indexPath: IndexPath) -> Student {
        
        var arrayOfStudents      = [Student]()
        let lastNameDiscriptor   = NSSortDescriptor(key: "lastName", ascending: true)
        let firstNameDiscriptor  = NSSortDescriptor(key: "firstName", ascending: true)
        
        for student in students {
            arrayOfStudents.append(student as! Student)
        }
        
        let sortedArray = (arrayOfStudents as NSArray).sortedArray(using: [firstNameDiscriptor,lastNameDiscriptor])
        let student     = sortedArray[indexPath.row] as! Student
        
        return student
    }

    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 && indexPath.row == 2 {
            
            let teacher = course?.teacher
            let vc      = TeacherInfoViewController()
            vc.teacher  = teacher
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if indexPath.section == 1 {
            
            let student = self.selectedStudent(students: course!.students!, indexPath: indexPath)
            let vc      = StudentInfoViewController()
            vc.student  = student
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
