//
//  StudentViewController.swift
//  CoreDataHW
//
//  Created by Admin on 27.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class StudentViewController: CoreDataViewController {
    
    override var fetchedResultsController : NSFetchedResultsController<NSManagedObject> {
        
        let fetchRequest : NSFetchRequest<Student> = Student.fetchRequest()
        let lastNameDiscriptor  = NSSortDescriptor(key: "lastName", ascending: true)
        let firstNameDiscriptor = NSSortDescriptor(key: "firstName", ascending: true)
        
        fetchRequest.fetchBatchSize  = 20
        fetchRequest.sortDescriptors = [firstNameDiscriptor, lastNameDiscriptor]
        
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: super.context, sectionNameKeyPath: nil, cacheName: nil)//Создание контроллера
        
        controller.delegate = self
        
        do {
            try controller.performFetch()
        } catch {
            print("Can't perform fetch")
        }
        
        return controller as! NSFetchedResultsController<NSManagedObject>
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - MyMethods
    
    override func insertObject() {
        
        let vc = UITableViewController(style: .plain)
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func configureCell(cell: UITableViewCell, withEvent event: NSManagedObject) {
        cell.textLabel?.text = "\((event as! Student).firstName ?? "No firstName") \((event as! Student).lastName ?? "No lastName")"
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let student = self.fetchedResultsController.object(at: indexPath) as! Student
        let vc      = StudentInfoViewController()
        vc.student  = student
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
