//
//  StudentInfoViewController.swift
//  CoreDataHW
//
//  Created by Admin on 29.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit

class StudentInfoViewController: UITableViewController {

    var student : Student?
    let attributesOfStudent = 3
    
    init() {
        super.init(style: .plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = (student?.firstName ?? "No firstName") + " " + (student?.lastName ?? "No lastName")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section {
        case 0:
            return "Student info"
        default:
            return "Courses"
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return attributesOfStudent
        default:
            return student?.courses?.count ?? 0
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let infoCell = UITableViewCell(style: .value1, reuseIdentifier: "InfoCell")
            
            if indexPath.row == 0 {
                infoCell.textLabel!.text = "First name"
                infoCell.detailTextLabel!.text = student?.firstName ?? "No first name"
            } else if indexPath.row == 1 {
                infoCell.textLabel!.text = "Last name"
                infoCell.detailTextLabel!.text = student?.lastName ?? "No last name"
            } else {
                infoCell.textLabel!.text = "E-mail"
                infoCell.detailTextLabel!.text = student?.email ?? "No E-mail"
            }
            return infoCell
            
        default:
            let courseCell = UITableViewCell(style: .default, reuseIdentifier: "CoursesCell")
            
            if student?.courses != nil {
                let course = self.selectedCourse(courses: (student?.courses!)!, indexPath: indexPath)
                courseCell.textLabel!.text = course.name ?? "No name"
            }
            return courseCell
        }
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    // MARK: - MyMethods
    
    func selectedCourse(courses: NSSet, indexPath: IndexPath) -> Course {
        
        var arrayOfCourses = [Course]()
        let nameDiscriptor = NSSortDescriptor(key: "name", ascending: true)
        
        for course in courses {
            arrayOfCourses.append(course as! Course)
        }
        
        let sortedArray = (arrayOfCourses as NSArray).sortedArray(using: [nameDiscriptor])
        let course      = sortedArray[indexPath.row] as! Course
        
        return course
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            
            let course  = self.selectedCourse(courses: (student?.courses!)!, indexPath: indexPath)
            let vc      = CourseInfoViewController()
            vc.course   = course
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
