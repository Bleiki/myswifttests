//
//  AppDelegate.swift
//  CoreDataHW
//
//  Created by Admin on 27.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    
    let firstNames = [
        "Tran", "Lenore", "Bud", "Fredda", "Katrice",
        "Clyde", "Hildegard", "Vernell", "Nellie", "Rupert",
        "Billie", "Tamica", "Crystle", "Kandi", "Caridad",
        "Vanetta", "Taylor", "Pinkie", "Ben", "Rosanna",
        "Eufemia", "Britteny", "Ramon", "Jacque", "Telma",
        "Colton", "Monte", "Pam", "Tracy", "Tresa",
        "Willard", "Mireille", "Roma", "Elise", "Trang",
        "Ty", "Pierre", "Floyd", "Savanna", "Arvilla",
        "Whitney", "Denver", "Norbert", "Meghan", "Tandra",
        "Jenise", "Brent", "Elenor", "Sha", "Jessie" ]
    
    let lastNames = [
        "Farrah", "Laviolette", "Heal", "Sechrest", "Roots",
        "Homan", "Starns", "Oldham", "Yocum", "Mancia",
        "Prill", "Lush", "Piedra", "Castenada", "Warnock",
        "Vanderlinden", "Simms", "Gilroy", "Brann", "Bodden",
        "Lenz", "Gildersleeve", "Wimbish", "Bello", "Beachy",
        "Jurado", "William", "Beaupre", "Dyal", "Doiron",
        "Plourde", "Bator", "Krause", "Odriscoll", "Corby",
        "Waltman", "Michaud", "Kobayashi", "Sherrick", "Woolfolk",
        "Holladay", "Hornback", "Moler", "Bowles", "Libbey",
        "Spano", "Folson", "Arguelles", "Burke", "Rook"
    ]
    
    let courseNames      = ["IOS", "Java", "SQL", "Swift", "Objective-C", "Relam", "HTML", "Piton"]
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        /*
        let studentEntity = NSEntityDescription.entity(forEntityName: "Student", in: self.persistentContainer.viewContext)
        let courseEntity  = NSEntityDescription.entity(forEntityName: "Course", in: self.persistentContainer.viewContext)
        let teacherEntity = NSEntityDescription.entity(forEntityName: "Teacher", in: self.persistentContainer.viewContext)
        
        for _ in 0...4 {
            
            let teacher       = Teacher(entity: teacherEntity!, insertInto: self.persistentContainer.viewContext)
            teacher.direction = Int(arc4random_uniform(2)) == 1 ? "Development" : "Programming language"
            teacher.firstName = firstNames[Int(arc4random_uniform(50))]
            teacher.lastName  = lastNames[Int(arc4random_uniform(50))]
            teacher.email     = teacher.lastName! + "@mail.ru"
            
            for _ in 0...1 {
                
                let course       = Course(entity: courseEntity!, insertInto: self.persistentContainer.viewContext)
                course.teacher   = teacher
                course.name      = courseNames[Int(arc4random_uniform(8))] + " from " + teacher.firstName!
                course.direction = teacher.direction
                
                for _ in 0...(Int(arc4random_uniform(25)) + 15) {
                    
                    let student       = Student(entity: studentEntity!, insertInto: self.persistentContainer.viewContext)
                    student.lastName  = lastNames[Int(arc4random_uniform(50))]
                    student.firstName = firstNames[Int(arc4random_uniform(50))]
                    student.email     = student.lastName! + "@mail.ru"
                    student.addToCourses(course)
                }
            }
        }
        
        do {
            try self.persistentContainer.viewContext.save()
        } catch {
            print("Can't save data")
        }
        */
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        DataManager.instance.saveContext()
    }

}

