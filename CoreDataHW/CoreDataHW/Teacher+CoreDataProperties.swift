//
//  Teacher+CoreDataProperties.swift
//  CoreDataHW
//
//  Created by Admin on 28.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation
import CoreData


extension Teacher {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Teacher> {
        return NSFetchRequest<Teacher>(entityName: "Teacher");
    }

    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var email: String?
    @NSManaged public var direction: String?
    @NSManaged public var courses: NSSet?

}

// MARK: Generated accessors for courses
extension Teacher {

    @objc(addCoursesObject:)
    @NSManaged public func addToCourses(_ value: Course)

    @objc(removeCoursesObject:)
    @NSManaged public func removeFromCourses(_ value: Course)

    @objc(addCourses:)
    @NSManaged public func addToCourses(_ values: NSSet)

    @objc(removeCourses:)
    @NSManaged public func removeFromCourses(_ values: NSSet)

}
