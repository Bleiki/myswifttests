//
//  CourseViewController.swift
//  CoreDataHW
//
//  Created by Admin on 28.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class CourseViewController: CoreDataViewController {

    override var fetchedResultsController : NSFetchedResultsController<NSManagedObject> {
        
        let fetchRequest : NSFetchRequest<Course> = Course.fetchRequest()
        let nameDiscriptor  = NSSortDescriptor(key: "name", ascending: true)
        
        fetchRequest.fetchBatchSize  = 20
        fetchRequest.sortDescriptors = [nameDiscriptor]
        
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: super.context, sectionNameKeyPath: nil, cacheName: nil)//Создание контроллера
        
        controller.delegate = self
        
        do {
            try controller.performFetch()
        } catch {
            print("Can't perform fetch")
        }
        
        return controller as! NSFetchedResultsController<NSManagedObject>
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - MyMethods
    
    override func insertObject() {
        
        let vc = UITableViewController(style: .plain)
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func configureCell(cell: UITableViewCell, withEvent event: NSManagedObject) {
        cell.textLabel?.text = (event as! Course).name ?? "No name"
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let course = self.fetchedResultsController.object(at: indexPath) as! Course
        let vc     = CourseInfoViewController()
        vc.course  = course
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
