
print("Max Int8: \(Int8.max)")
print("Min Int8: \(Int8.min)")
print("Max UInt8: \(UInt8.max)")
print("Max UInt8: \(UInt8.min)")

print("Max Int16: \(Int16.max)")
print("Min Int16: \(Int16.min)")
print("Max UInt16: \(UInt16.max)")
print("Max UInt16: \(UInt16.min)")

print("Max Int32: \(Int32.max)")
print("Min Int32: \(Int32.min)")
print("Max UInt32: \(UInt32.max)")
print("Max UInt32: \(UInt32.min)")

print("Max Int64: \(Int64.max)")
print("Min Int64: \(Int64.min)")
print("Max UInt64: \(UInt64.max)")
print("Max UInt64: \(UInt64.min)")

let a = 1
let b : Float = 1.5
let c = 1.7

let sumI = Int(Double(a) + Double(b) + c)
let sumF = Float(a) + b + Float(c)
let sumD = Double(a) + Double(b) + c

print("Numbers: \nInt: \(sumI) \nFloat: \(sumF) \nDouble: \(sumD)")

if Double(sumI) > sumD {
    print("Int is greater double")
} else if Double(sumI) == sumD {
    print("Int is equal double")
} else {
    print("Double is greater Int")
}





