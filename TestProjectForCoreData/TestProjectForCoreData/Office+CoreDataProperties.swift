//
//  Office+CoreDataProperties.swift
//  TestProjectForCoreData
//
//  Created by Admin on 25.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation
import CoreData


extension Office {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Office> {
        return NSFetchRequest<Office>(entityName: "Office");
    }

    @NSManaged public var name: String?
    @NSManaged public var company: Company?
    @NSManaged public var personal: NSSet?
    @NSManaged public var youngPersonal : [Person]

}

// MARK: Generated accessors for personal
extension Office {

    @objc(addPersonalObject:)
    @NSManaged public func addToPersonal(_ value: Person)

    @objc(removePersonalObject:)
    @NSManaged public func removeFromPersonal(_ value: Person)

    @objc(addPersonal:)
    @NSManaged public func addToPersonal(_ values: NSSet)

    @objc(removePersonal:)
    @NSManaged public func removeFromPersonal(_ values: NSSet)

}
