//
//  Work+CoreDataProperties.swift
//  TestProjectForCoreData
//
//  Created by Admin on 25.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation
import CoreData


extension Work {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Work> {
        return NSFetchRequest<Work>(entityName: "Work");
    }


}
