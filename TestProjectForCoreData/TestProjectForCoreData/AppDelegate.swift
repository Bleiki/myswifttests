//
//  AppDelegate.swift
//  TestProjectForCoreData
//
//  Created by Admin on 22.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import CoreData
import Foundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    //MARK: - Properties
    let names       = ["Tran", "Lenore", "Bud", "Fredda", "Katrice", "Clyde", "Hildegard", "Vernell", "Nellie", "Rupert"]
    let carNames    = ["Dodge", "Toyota", "BMW", "Lada", "Reno"]
    let officeNames = ["SPB Office", "Moscow Office", "Rostov Office", "Kaluga Office", "NewYork Office"]
    
    //MARK: - GetFunctions
    func allObjects() -> [NSManagedObject] {
        
        var objects = [NSManagedObject]()

        do {
            objects = try self.persistentContainer.viewContext.fetch(Work.fetchRequest())//Получение данных из КорДаты
        } catch {
            print("Can't get data")
        }
       
        return objects
    }
    
    func allPersons() -> [NSManagedObject] {
        
        var persons = [NSManagedObject]()
        
        do {
            persons = try self.persistentContainer.viewContext.fetch(Person.fetchRequest())//Получение данных из КорДаты
        } catch {
            print("Can't get data")
        }
        
        return persons
    }
    
    func allOffices() -> [NSManagedObject] {
        
        var offices = [NSManagedObject]()
        
        do {
            offices = try self.persistentContainer.viewContext.fetch(Office.fetchRequest())//Получение данных из КорДаты
        } catch {
            print("Can't get data")
        }
        
        return offices
    }
    
    func allCompanies() -> [NSManagedObject] {
        
        var companies = [NSManagedObject]()
        
        do {
            companies = try self.persistentContainer.viewContext.fetch(Company.fetchRequest())//Получение данных из КорДаты
        } catch {
            print("Can't get data")
        }
        
        return companies
    }
    
    func printCounts() {
        
        let allObjects   = self.allObjects()
        let allOffices   = self.allOffices()
        let allPersons   = self.allPersons()
        let allCompanies = self.allCompanies()

        print("Companies: \(allCompanies.count)\nOffices: \(allOffices.count)\nPersons: \(allPersons.count)\nObjects: \(allObjects.count)")
    }
    
    func printObjects(objects: [NSManagedObject]) {
        
        for object in objects {
            
            if object.isKind(of: Person.self) {
                print("Name: \((object as! Person).name!), Age: \((object as! Person).age), Car: \((object as! Person).car!)")
            } else if object.isKind(of: Office.self) {
                print("Name: \((object as! Office).name!), Young Personal: \((object as! Office).youngPersonal.count)")
            } else if object.isKind(of: Company.self) {
                print("Name: \((object as! Company).name!)")
            } else {
                print("I can't print(((")
            }
        }
    }
    
    func saveData() {
        //Сохранение в базу
        do {
            try self.persistentContainer.viewContext.save()
        } catch {
            print("Can't save data")
        }
    }
    
    func deleteAllObjects() {
        
        let objects = self.allObjects()
        
        //Помещение на удаление
        for object in objects {
            self.persistentContainer.viewContext.delete(object)
        }
        
        self.saveData()
    }
    
    //MARK: - CreateFunctions
    func addPerson() -> NSManagedObject {
        
        //Описание сущности
        let entity = NSEntityDescription.entity(forEntityName: "Person", in: self.persistentContainer.viewContext)
        //Создание объекта сущности
        let person = Person(entity: entity!, insertInto: self.persistentContainer.viewContext)
        
        person.setValue(names[Int(arc4random_uniform(10))], forKey: "name")
        person.setValue(carNames[Int(arc4random_uniform(5))], forKey: "car")
        person.setValue(Int32(arc4random_uniform(11) + 20), forKey: "age")
        
        return person
    }
    
    func addOffice() -> NSManagedObject {
        
        //Описание сущности
        let entity = NSEntityDescription.entity(forEntityName: "Office", in: self.persistentContainer.viewContext)
        //Создание объекта сущности
        let office = Office(entity: entity!, insertInto: self.persistentContainer.viewContext)
        
        office.setValue(officeNames[Int(arc4random_uniform(5))], forKey: "name")
        
        return office
    }
    
    func addCompany() -> NSManagedObject {
        
        //Описание сущности
        let entity  = NSEntityDescription.entity(forEntityName: "Company", in: self.persistentContainer.viewContext)
        //Создание объекта сущности
        let company = Company(entity: entity!, insertInto: self.persistentContainer.viewContext)
        
        company.name = "Gazprom"
        
        return company
    }

    //MARK: - StandartFunctions
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let company = self.addCompany()
        
        for _ in 0...4 {
            
            let office = self.addOffice()
            office.setValue(company, forKey: "company")
            
            for _ in 0...7 {
                let person = self.addPerson()
                person.setValue(company, forKey: "company")
                person.setValue(office, forKey: "office")
            }
        }
        
        //Помещение объекта на удаление(удалится только после вызова фнкции сохранения(save)
        self.persistentContainer.viewContext.delete(allOffices().last!)
        
        var objectsOnRequest = [NSManagedObject]()
        let nameDiscriptor   = NSSortDescriptor(key: "name", ascending: true)
        let ageDiscriptor    = NSSortDescriptor(key: "age", ascending: true)
        var oldPersons       = [NSManagedObject]()
        let predicate        = NSPredicate(format: "age >= 26 AND age <= %@ AND car = %@", argumentArray: [28, "Dodge"]) //Установка фильтра. Можно вместо AND(OR) использовать &&(||). Так-же есть ANY, IN. Из функций:@sum, @max, @min, @avg, @count. @"SUBQUERY(students, $student, $student.car.model == "BMW").@count >= 6  Подзапрос (Для сложной задачи). (Над какой проперти выполняется, $какой объект, $условие) Можно сказать создался цикл для объекта student в students. SUBQUERY() вернет массив. В данном запросе запрошивается группа, в которой хотябы у 6 студентов машина BMW.
        let request          = NSFetchRequest<NSManagedObject>(entityName: "Person")
        var standartRequest  = NSFetchRequest<NSManagedObject>()
            
        standartRequest  = (self.persistentContainer.managedObjectModel.fetchRequestTemplate(forName: "OldPersons"))?.copy() as! NSFetchRequest<NSManagedObject> //Для работы с FetchRequest из файла корДаты. Позволяет выбрать фильтр из файла кор даты.(В большинстве случаев для часо повторяющихся запросов). Для того, чтобы можно было устанавливать NSSortDescriptor, нужно создать КОПИЮ.
        
        standartRequest.sortDescriptors = [ageDiscriptor, nameDiscriptor]
        
        request.relationshipKeyPathsForPrefetching = ["Office"] //Вызывает предзагрузку данных, например для того, чтобы при установке свойства оффис объекту студент, для каждого студента каждый раз не делался запрос к оффису
        
        request.sortDescriptors = [nameDiscriptor, ageDiscriptor] //Установка метода сортировки
        
        request.fetchBatchSize = 8 //Устанавливает количество объектов, данные которых загрузятся в первой пачке. Как только прейдем к объекту, данные которого не загружены, загрузит еще столько-же
        
        request.fetchOffset = 1 //Отступ, говорит с какого объекта начнем загружать(со 2 в данном случае)
        
        request.fetchLimit = 33 //Устанавливает макс колличество объектов, которые будут взяты(не больше)
        
        request.predicate = predicate //Непосредственно фильтрация извлекаемых данных
        
        do {
            objectsOnRequest = try self.persistentContainer.viewContext.fetch(request)
            oldPersons       = try self.persistentContainer.viewContext.fetch(standartRequest)
        } catch {
            print("I can't get DATA at that request")
        }
        
        self.saveData()
        
        //self.deleteAllObjects()
        self.printCounts()
        self.printObjects(objects: objectsOnRequest)
        self.printObjects(objects: oldPersons)
        
        //FetchedProperty: нужны для постановки свойств(при создании подкласса эти свойства сами не добавятся, их надо добавлять самим с типом [типХранимыхДанных]). Для того чтобы в файле кор даты поставить условие, в котором будет учавствовать одно из свойст сущности, надо написать $FETCHED_SOURCE.названиеПроперти. При изменении условия надо презагрузить объект.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Model")//Название файла кор даты
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

