//
//  Person+CoreDataProperties.swift
//  TestProjectForCoreData
//
//  Created by Admin on 25.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation
import CoreData


extension Person {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person");
    }

    @NSManaged public var age: Int32
    @NSManaged public var car: String?
    @NSManaged public var name: String?
    @NSManaged public var company: Company?
    @NSManaged public var office: Office?

}
