//
//  Company+CoreDataProperties.swift
//  TestProjectForCoreData
//
//  Created by Admin on 25.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import Foundation
import CoreData


extension Company {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Company> {
        return NSFetchRequest<Company>(entityName: "Company");
    }

    @NSManaged public var name: String?
    @NSManaged public var offices: NSSet?
    @NSManaged public var personal: NSSet?

}

// MARK: Generated accessors for offices
extension Company {

    @objc(addOfficesObject:)
    @NSManaged public func addToOffices(_ value: Office)

    @objc(removeOfficesObject:)
    @NSManaged public func removeFromOffices(_ value: Office)

    @objc(addOffices:)
    @NSManaged public func addToOffices(_ values: NSSet)

    @objc(removeOffices:)
    @NSManaged public func removeFromOffices(_ values: NSSet)

}

// MARK: Generated accessors for personal
extension Company {

    @objc(addPersonalObject:)
    @NSManaged public func addToPersonal(_ value: Person)

    @objc(removePersonalObject:)
    @NSManaged public func removeFromPersonal(_ value: Person)

    @objc(addPersonal:)
    @NSManaged public func addToPersonal(_ values: NSSet)

    @objc(removePersonal:)
    @NSManaged public func removeFromPersonal(_ values: NSSet)

}
