//: Playground - noun: a place where people can play

import UIKit
import Foundation
import Darwin

var str = "Hello, playground"

class Student {
    
    static let currentYear : Int = 2017
    
    let birthYear : Int
    
    var age : Int {
        
        return Student.currentYear - birthYear
    }
    
    var learning : Int {
        
        if age > 6 {
            return age - 6
        } else {
            return 0
        }
    }
    
    init(birthYear: Int) {
        self.birthYear = birthYear
    }
}

let student = Student(birthYear: 1994)

student.age
student.learning

struct LineSegment {
    
    struct Point{
        let x : Int
        let y : Int
    }
    
    var pointA : Point
    var pointB : Point
    
    var length : Double {
        return sqrt(pow(Double(pointB.x - pointA.x), 2) + pow(Double(pointB.y - pointA.y), 2))
    }
    
    var midPoint : Point {
        return Point(x: (pointA.x + pointB.x) / 2, y: (pointA.y - pointB.y) / 2)
    }
}

var lineSegment = LineSegment(pointA: LineSegment.Point(x: 1, y: 2), pointB: LineSegment.Point(x: 3, y: 5))

lineSegment.length
lineSegment.midPoint






struct MyFile {
    
    static let maxSize = 2
    
    var fileName : String
    var pathToFolder : String
    var isHidden: Bool
    var fileContent : String
    
    var pathToFile : String {
        return (pathToFolder + "/" + fileName)
    }
    
}

let file = MyFile(fileName: "Game", pathToFolder: "/Comp/Start/freedom", isHidden: false, fileContent: "Hi, i'm game")

file.pathToFile


class Human {
    
    static let minAge        = 18
    static let maxAge        = 73
    static let minNameLength = 4
    static let maxNameLength = 15
    static var numberOfHuman = 0
    
    
    var firstName : String {
        didSet {
            if (firstName as NSString).length > Human.maxNameLength || (firstName as NSString).length < Human.maxNameLength {
                firstName = oldValue
            }
        }
    }
    
    var age : Int {
        didSet {
            if age < Human.minAge || age > Human.maxAge {
                age = oldValue
            }
        }
    }
    
    var lastName : String
    var growth   : Float
    var weight   : Float
    
    init(firstName: String, lastName: String, age: Int, growth: Float, weight: Float) {
        
        self.firstName = firstName
        self.lastName  = lastName
        self.age       = age
        self.growth    = growth
        self.weight    = weight
        
        Human.numberOfHuman += 1
    }
    
}

var man = Human.init(firstName: "Slava", lastName: "Gerasimenok", age: 22, growth: 182.4, weight: 72.3)

man.age = 11
man.age

Human.numberOfHuman











