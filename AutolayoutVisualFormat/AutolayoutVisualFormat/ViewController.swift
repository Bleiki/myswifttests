//
//  ViewController.swift
//  AutolayoutVisualFormat
//
//  Created by Admin on 28.02.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    
    /*
     
     Устанавливаются констраинты с помощью метода класса NSLAyoutConstraint: constraintsWithVisualFormat:options:metrics:views: .
     
     class func constraintsWithVisualFormat(_ format: String,
                                    options opts: NSLayoutFormatOptions, //Обычно nil
                                    metrics metrics: [NSObject : AnyObject]?,
                                        views views: [NSObject : AnyObject]) -> [AnyObject]
     
     
     [view1]-[view2] //Левая сторона view2 должна находиться правее левой стороны view1 на расстоянии по умолчанию (8 пикселей).
     
     [view1]-10-[view2] //Расстояние между вьюхами 10 пикселей ("-" устанавливает расстояние и по умолчанию по горизонтали). Для читаемости можно писать "Н:".
     
     V: [view1]-10-[view2] //Вертикальная связь (на это указывает "V:")
     
     |-10-[textField]-10-| //"|" oзначает superview. Правая и левые стороны textField д.б. закреплены на 10 пикселей от родительского представления (от superview).
     
     [view1(100)] //Размер view1 д.б. 100 пикселей.
     
     [button1(100)]-20-[button2(==button1)] //Ширина button1 100 пикселей. Ширина button2 равна ширине button1. Кнопки должны находиться на расстоянии 20 пикселей.
     
     [view1] [view2] //Flush. А именно вьюхи прилегают вплотную друг к другу.
     
     [view1(100@20)] //Вью размерами 100 с приоритетом 20.
     
     [button(>=70,<=100)] //Ширина больше или равна 70 и меньше или равна 100.
     
    */
    
    var headerView  : UIView!
    var titleLabel  : UILabel!
    var descLabel   : UILabel!
    var imageView   : UIImageView!
    var buttonStart : UIButton!
    
    @IBOutlet weak var moveImageView: UIImageView!
    @IBOutlet weak var horizontal: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        initViews()
        createVFL()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func initViews() {
        
        self.headerView = UIView()
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.backgroundColor = UIColor.lightGray
        self.view.addSubview(self.headerView)
        
        self.titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = UIFont.systemFont(ofSize: 10.0)
        titleLabel.numberOfLines = 0
        titleLabel.preferredMaxLayoutWidth = 150.0
        titleLabel.backgroundColor = UIColor.yellow
        titleLabel.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda."
        headerView.addSubview(titleLabel)
        
        self.descLabel = UILabel()
        descLabel.translatesAutoresizingMaskIntoConstraints = false
        descLabel.font = UIFont.systemFont(ofSize: 14.0)
        descLabel.backgroundColor = UIColor.green
        descLabel.text = "Author: Viacheslav"
        headerView.addSubview(descLabel)
        
        self.imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = UIViewContentMode.scaleAspectFit //Чтобы картинка заняла все поле
        imageView.backgroundColor = UIColor.red
        headerView.addSubview(imageView)
        
        self.buttonStart = UIButton(type: .system)
        buttonStart.translatesAutoresizingMaskIntoConstraints = false
        buttonStart.setTitle("Start", for: .normal)
        buttonStart.addTarget(self, action: #selector(startMove), for: .touchUpInside)
        headerView.addSubview(buttonStart)
        
    }
    
    func startMove() {
        
        self.buttonStart.isEnabled = false
        
        UIView.animateKeyframes(withDuration: 5, delay: 0, options: .calculationModeCubicPaced, animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.5, animations: {
                self.horizontal.constant = 200
                self.view.layoutIfNeeded()
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.25, animations: {
                self.horizontal.constant = 25
                self.view.layoutIfNeeded()
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.75, relativeDuration: 0.125, animations: {
                self.horizontal.constant = 200
                self.view.layoutIfNeeded()
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.875, relativeDuration: 0.0625, animations: {
                self.horizontal.constant = 25
                self.view.layoutIfNeeded()
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.9375, relativeDuration: 0.3125, animations: {
                self.horizontal.constant = 200
                self.view.layoutIfNeeded()
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.96875, relativeDuration: 0.015625, animations: {
                self.horizontal.constant = 25
                self.view.layoutIfNeeded()
            })
            
        }) { (finished: Bool) in
            self.buttonStart.isEnabled = true
        }
    }
    
    func createVFL() {
        
        let views = Dictionary(dictionaryLiteral: ("headerView", headerView), ("titleLabel", titleLabel), ("descLabel", descLabel), ("imageView", imageView), ("buttonStart", buttonStart)) //Все наши представления
        
        let metrics = Dictionary(dictionaryLiteral: ("imageWidth", 100), ("imageHeight", 200), ("padding", 15)) //padding - отступ
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[headerView]|", options: [], metrics: metrics, views: views))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-35-[headerView]", options: [], metrics: metrics, views: views)) //В конце не указывается привязка к родителю, чтобы вьюха не растягивалась до конца, а растягивалась в зависимости от размеров сабвьюх.
        
        headerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-padding-[imageView(imageWidth)]-padding-[titleLabel]-padding-|", options: [], metrics: metrics, views: views))
        headerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-padding-[titleLabel]->=0-[descLabel]-padding-|", options: [.alignAllRight], metrics: metrics, views: views))
        headerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-padding-[imageView(imageHeight)]->=padding-[buttonStart]-padding-|", options: [.alignAllCenterX], metrics: metrics, views: views)) //options говорят как выравнен последний объект, указанный в Format.
        
        
    }

}

