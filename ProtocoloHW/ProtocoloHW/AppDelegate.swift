//
//  AppDelegate.swift
//  ProtocoloHW
//
//  Created by Admin on 25.02.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import Foundation

protocol Food {
    
    var name : String { get }
    func taste()
}

protocol Storable : Food {
    
    var expired : Bool { get }
    var daysToExpired : Int { get }
}

class Meat : NSObject, Storable {
    
    let name : String
    var expired: Bool
    var daysToExpired : Int
    
    func taste() {
        print("Hard")
    }
    
    init(name:String, expired:Bool, daysToExpired:Int) {
        self.name = name
        self.expired = expired
        self.daysToExpired = daysToExpired
    }
}

class Milk : NSObject, Storable {
    
    let name : String
    var expired: Bool
    var daysToExpired : Int
    
    func taste() {
        print("Liquid")
    }
    
    init(name:String, expired:Bool, daysToExpired:Int) {
        self.name = name
        self.expired = expired
        self.daysToExpired = daysToExpired
    }
}

class Tomate : NSObject, Food {
    
    let name : String
    
    func taste() {
        print("Juicy")
    }
    
    init(name:String) {
        self.name = name
    }
}

class Pasta : NSObject, Food {
    
    let name : String
    
    func taste() {
        print("Flashily")
    }
    
    init(name:String) {
        self.name = name
    }
}

class Bread : NSObject, Food {
    
    let name : String
    
    func taste() {
        print("Dry")
    }
    
    init(name:String) {
        self.name = name
    }
}

class Pelmeni : NSObject, Storable {
    
    let name : String
    var expired : Bool
    var daysToExpired : Int
    
    func taste() {
        print("Hot")
    }
    
    init(name:String, expired:Bool, daysToExpired:Int) {
        self.name = name
        self.expired = expired
        self.daysToExpired = daysToExpired
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let meat = Meat(name: "Meat", expired: true, daysToExpired: 0)
        let milk = Milk(name: "Milk", expired: false, daysToExpired: 6)
        let tomate = Tomate(name: "Tomate")
        let pasta = Pasta(name: "Pasta")
        let bread = Bread(name: "Bread")
        let pelmeni = Pelmeni(name: "Pelmeni", expired: false, daysToExpired: 2)
        
        let bag = [meat, milk, tomate, pasta, bread, pelmeni] as [Food]
        let nameDiscriptor = NSSortDescriptor(key: "name", ascending: true)
        var sortBag = (bag as NSArray).sortedArray(using: [nameDiscriptor]) as! [Food]
        
        var deleteObjects = [Int]()
        var fridge = [Storable]()
        
        for i in 0...sortBag.count - 1 {
            let obj = sortBag[i]
            if let newObj = obj as? Storable {
                if !newObj.expired {
                    fridge.append(newObj)
                } else {
                    deleteObjects.append(i)
                }
            }
        }
        
        for index in deleteObjects {
            sortBag.remove(at: index)
        }
        
        let daysSortDiscriptor = NSSortDescriptor(key: "daysToExpired", ascending: true)
        let sortFridge = (fridge as NSArray).sortedArray(using: [daysSortDiscriptor, nameDiscriptor]) as! [Food]
        
        printFood(array: sortBag)
        print("_________________________")
        printFood(array: sortFridge)
        
        return true
        
    }
    
    func printFood(array: [Food]) {
        
        for obj in array {
            print(obj.name)
            obj.taste()
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

