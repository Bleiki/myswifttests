
var journal = ["Slava":4, "Petya":3, "Olya":5, "Vika":5, "Pasha":2]

print(journal)

journal["Slava"] = 5
journal.updateValue(3, forKey: "Pasha")

print(journal)

journal["Egor"] = 2
journal["Jon"] = 4

print(journal)

journal["Pasha"] = nil
journal.removeValue(forKey: "Olya")

print(journal)

var overallScore = 0

for (key:value) in journal {
    
    overallScore += value
}

let averageScore = overallScore / journal.count

print("overallScore: \(overallScore) \naverageScore: \(averageScore)")

let daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
let monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

var dates = [String:Int]()

for i in 0..<daysInMonth.count {
    dates[monthNames[i]] = daysInMonth[i]
}

print(dates)

for (key:value) in dates {
    print("\(key):\(value)")
}

for (key:value) in dates {
    print(dates[key])
}

