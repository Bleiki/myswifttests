//
//  VGStudent.swift
//  KVC HomeWork
//
//  Created by Admin on 22.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit


class VGStudent: NSObject {
    
    enum Gender {
        
        case Man
        case Woman
    }
    
    var birthYear : Int
    var firstName : String
    var lastName  : String
    var gender    : Gender
    var grade     : Float
    
    init(birthYear: Int, firstName: String, lastName: String, gender: Gender, grade: Float) {
        
        self.birthYear = birthYear
        self.firstName = firstName
        self.lastName  = lastName
        self.gender    = gender
        self.grade     = grade
    }
    
    
    override func setValue(_ value: Any?, forKeyPath keyPath: String) {
        
        print("setValue: \(value) for keyPath: \(keyPath)")
        
        super.setValue(value, forKeyPath: keyPath)
    }
    
}
