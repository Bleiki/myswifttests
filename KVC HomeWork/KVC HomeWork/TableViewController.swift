//
//  ViewController.swift
//  KVC HomeWork
//
//  Created by Admin on 22.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

import UIKit
import CoreFoundation


class TableViewController: UITableViewController {
    
    @IBOutlet weak var gradeField    : UITextField!
    @IBOutlet weak var birthField    : UITextField!
    @IBOutlet weak var genderControl : UISegmentedControl!
    @IBOutlet weak var lastNameField : UITextField!
    @IBOutlet weak var firstNameField: UITextField!
    
    var student = VGStudent(birthYear: 1994, firstName: "Vyacheslav", lastName: "Gerasimenok", gender: .Man, grade: 4.4)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let inset   = UIEdgeInsetsMake(20, 0, 0, 0)
        
        self.tableView.scrollIndicatorInsets = inset
        self.tableView.contentInset          = inset
        
        self.student.addObserver(self, forKeyPath: "birthYear", options: [.new, .old], context: nil)

        self.firstNameField.text  = self.student.firstName
        self.lastNameField.text   = self.student.lastName
        self.birthField.text      = String(self.student.birthYear)
        self.gradeField.text      = String(self.student.grade)
        
        switch self.student.gender {
        case .Man:
            self.genderControl.selectedSegmentIndex = 0
        default:
            self.genderControl.selectedSegmentIndex = 1
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        print("\nKeyPath: \(keyPath) \nObject: \(object) \nChange: \(change)")
    }
    
   
    @IBAction func fieldExit(_ sender: UITextField) {
        
        switch sender {
            
        case self.firstNameField:
            self.student.setValue(sender.text!, forKeyPath: "firstName")
            print("firstName: " + self.student.firstName)
            
        case self.lastNameField:
            self.student.setValue(sender.text!, forKeyPath: "lastName")
            print("lastName: " + self.student.lastName)
            
        case self.birthField:
            self.student.setValue(sender.text!, forKeyPath: "birthYear")
            print("birthYear: " + "\(self.student.birthYear)")
            
        default:
            self.student.setValue(sender.text!, forKeyPath: "grade")
            print("grade: " + "\(self.student.grade)")
        }
    }

    
    @IBAction func valueChange(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            self.student.gender = .Man
        default:
            self.student.gender = .Woman
        }
        print("Gender: " + "\(self.student.gender)")
    }


}

